package com.company;

public class Candy implements Comparable<Candy> {
    private String name;
    private double weight;
    private double sugarConcentration;
    private double price;

    public Candy() {
    }

    public Candy(String name, double weight, double sugarConcentration, double price) {
        this.name = name;
        this.weight = weight;
        this.sugarConcentration = sugarConcentration;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getSugarConcentration() {
        return sugarConcentration;
    }

    public void setSugarConcentration(double sugarConcentration) {
        this.sugarConcentration = sugarConcentration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int compareTo(Candy other) {
        return Double.compare(weight, other.weight);
    }

    @Override
    public boolean equals(Object o) {
        Candy candy = (Candy) o;
        if (Double.compare(candy.weight, weight) != 0) return false;
        return name != null ? name.equals(candy.name) : candy.name == null;
    }

    @Override
    public String toString() {
        return String.format(
                "[Name: %s; Weight: %f; Sugar: %f; Price: %f]",
                name, weight, sugarConcentration, price
        );
    }
}
