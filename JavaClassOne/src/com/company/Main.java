package com.company;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<Candy> candies = new TreeSet<>();
        candies.add(new Candy("Oreo", 0.4, 0.365, 400));
        candies.add(new Candy("Mars", 0.45, 0.25, 500));
        candies.add(new Candy("Albeni", 1.15, 0.4, 200));
        candies.add(new Candy("Bounty", 0.14, 0.5, 350));
        candies.add(new Candy("Nuts", 0.42, 0.35, 370));
        candies.add(new Candy("Kitkat", 0.85, 0.15, 580));
        candies.add(new Candy("Snickers", 0.35, 0.45, 770));
        candies.add(new Candy("Lion", 1.2, 0.3, 390));
        candies.add(new Candy("Oreo", 0.4, 0.365, 400));
        candies.add(new Candy("Mars", 0.45, 0.25, 500));
        candies.add(new Candy("Albeni", 1.15, 0.4, 200));
        candies.add(new Candy("Bounty", 0.14, 0.5, 350));
        candies.add(new Candy("Nuts", 0.42, 0.35, 370));
        candies.add(new Candy("Kitkat", 0.85, 0.15, 580));
        candies.add(new Candy("Snickers", 0.35, 0.45, 770));
        candies.add(new Candy("Lion", 1.2, 0.3, 390));

        Present gift = new Present(candies);

        System.out.println(String.format("Weight of the present: %.2f", gift.calculateWeight()));
        System.out.println(String.format("Price of the present: %.2f", gift.calculatePrice()));

        List<Candy> sortedCandies = gift.sortCandiesByWeight(true);

        System.out.println("Candies sorted by weight");
        for (Candy candy : sortedCandies) {
            System.out.println(candy);
        }

        double from = 0.14;
        double to = 0.20;
        Optional<Candy> foundCandy = gift.findCandyWithSugarConcentration(from, to);
        if (foundCandy.isPresent()) {
            System.out.println(String.format("Candy with sugar concentration from %.2f to %.2f - %s", from, to, foundCandy.get()));
        } else {
            System.out.println("Could not find the candy with mentioned parameters.");
        }
    }
}
