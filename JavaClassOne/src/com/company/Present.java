package com.company;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class Present {
    Set<Candy> candies;


    public Present() {
    }

    public Present(Set<Candy> candies) {
        this.candies = candies;
    }

    public Set<Candy> getCandies() {
        return candies;
    }

    public void setCandies(Set<Candy> candies) {
        this.candies = candies;
    }

    public double calculateWeight() {
        return candies.parallelStream().map(Candy::getWeight).reduce(0.0, (a, b) -> a + b);
    }

    public double calculatePrice() {
        return candies.parallelStream().map(Candy::getPrice).reduce(0.0, (a, b) -> a + b);
    }

    public List<Candy> sortCandiesByWeight() {
        return sortCandiesByWeight(true);
    }

    public List<Candy> sortCandiesByWeight(boolean ascending) {
        return ascending ?
                candies.parallelStream().sorted().collect(Collectors.toList()) :
                candies.parallelStream().sorted(Collections.reverseOrder()).collect(Collectors.toList());
    }

    public Optional<Candy> findCandyWithSugarConcentration(double from, double to) {
        return candies
                .parallelStream()
                .filter(candy -> candy.getSugarConcentration() >= from && candy.getSugarConcentration() <= to)
                .findFirst();
    }
}

